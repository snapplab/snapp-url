let {Url} = global.SnappFramework;

module.exports = function (self) {
  let to = this.Url;
  if (self.value.indexOf(to.value) === 0) {
    return Url(self.value.substr(to.value.length));
  }
};
