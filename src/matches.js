const SLASH = /\//g;
const SPLAT = /\*\*/g;
const PARAM = /\*|:\w+/g;

module.exports = function (self) {
  let candidate = this.Url;

  if (!candidate.hasOwnProperty('regex')) {
    let params = candidate.value.match(PARAM);
    candidate.params = !params ? [] : params.map(function (param) {
      return param.replace(':', '');
    });

    candidate.regex = new RegExp('^' + candidate.value
      .replace(SLASH, '\\/')
      .replace(SPLAT, '(.+)')
      .replace(PARAM, '([^\\/]+)') + '$');
  }

  // ignore ?search params
  let selfUrl = self.value.split('?').shift();
  let match = selfUrl.match(candidate.regex);
  if (match) {
    self.params = {};
    for (let i = 1; i < match.length; i++) {
      if (i > 0) {
        let name = candidate.params[i - 1];
        if (name === '*') {
          self.splat = match[i];
        }
        else {
          self.params[name] = match[i];
        }
      }
    }
    return true;
  }
  return false;
};
