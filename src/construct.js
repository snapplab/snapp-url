module.exports = function (self) {
  self.value = this.string;
  if (!self.value.length || self.value[0] !== '/') {
    self.value = `/${self.value}`;
  }
  self.value = self.value.replace(/\/{2,}/g, '/');
};
